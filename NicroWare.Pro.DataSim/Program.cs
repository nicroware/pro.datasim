using System;
using System.Net;
using System.Collections.Generic;
using System.Text;
using NicroWare.Lib.Networking;
using NicroWare.Lib.Sensors;
using System.IO;
using System.Net.Sockets;
using System.IO.Ports;

namespace NicroWare.Pro.CarGuiDataSim
{
    class MainClass
    {
        
        public static void Main(string[] args)
        {
            Runner.Run();
            Console.ReadKey();

            return;
            /*foreach (string s in SerialPort.GetPortNames())
            {
                Console.WriteLine(s);
            }*/
            SerialPort port = new SerialPort("/dev/ttyUSB0", 19200, Parity.None, 8, StopBits.One);

            UDPStreamClient streamClient = new UDPStreamClient(port, 150, null);
            streamClient.Connect(151);


            bool running = true;
            UdpClient client = new UdpClient(1510);
            client.Connect(IPAddress.Loopback, 1511);
            //client.Connect(IPAddress.Parse("10.0.0.3"), 1511);
            ushort[] sensorIDs = SensorLookup.SensorIDs;
            //Random rnd = new Random();
            Dictionary<ushort, OBJWrap<DataWrapper>> generatedData = new Dictionary<ushort, OBJWrap<DataWrapper>>();
            double counter = 0;
            int counter2 = 0;
            DateTime start = DateTime.Now;

            while ( running)
            {
                //byte[] tempArray = new byte[3 * sensorIDs.Length];
                byte[] tempArray = new byte[3 * (counter2 % 5 == 0 ? 8 : 2)];

                for (ushort i = 0; i < (counter2 % 5 == 0 ? 8 : 2)/*sensorIDs.Length*/; i++)
                {
                    if (!generatedData.ContainsKey(i))
                        generatedData.Add(i, new OBJWrap<DataWrapper>(new DataWrapper() { SensorID = i, Value = 0 }));
                    OBJWrap<DataWrapper> wrap = generatedData[i];
                    DataWrapper wrapper = generatedData[i].obj;
                    //wrapper.Value = (int)Math.Abs(Math.Sin(counter) * 200);
                    wrapper.Value = counter2;
                    //wrapper.Value += 1;//rnd.Next(0, 4); //rnd.Next(0, 6) - 2;
                    wrapper.Value = wrapper.Value % 200;
                    wrap.obj = wrapper;
                    byte[] temp = wrap.obj.GetBytes(true);
                    Array.Copy(temp, 0, tempArray, i * 3, temp.Length);
                    /*try
                    {
                        //client.Send(tempArray, tempArray.Length);
                        streamClient.Send(tempArray, tempArray.Length);
                    }
                    catch
                    {
                        running = false;
                    }*/
                    //System.Threading.Thread.Sleep(10);
                }

                try
                {
                    port.Open();
                    streamClient.Send(tempArray, tempArray.Length);
                    Console.WriteLine("Message nr: " + counter2 + " bytes to write: " + port.BytesToWrite);
                    port.Close();
                }
                catch
                {
                    running = false;
                }

                System.Threading.Thread.Sleep(100);
                counter += 0.005;
                counter2++;
            }
            port.Close();
            Console.WriteLine("CTS False error");
            Console.WriteLine("Runned for :" + (DateTime.Now - start).ToString());
            Console.ReadKey();
            return;
        }

        static void Port_PinChanged (object sender, SerialPinChangedEventArgs e)
        {
            Console.WriteLine(e);
        }

        static void Port_ErrorReceived (object sender, SerialErrorReceivedEventArgs e)
        {
            Console.WriteLine(e);
        }


    }

    public class OBJWrap<T>
    {
        public T obj { get; set; }

        public OBJWrap(T t)
        {
            this.obj = t;
        }
    }

    /* Old code
    public struct ShortIPProtocol
    {
        public uint SourceAddress { get; set; }
        public uint DestinationAddress { get; set; }
        //public readonly byte Zeros = 0;
        public Protocols protocol { get; set; }
        public int Length { get; set; }
    }

    public enum Protocols : byte
    {
        
    }

    public class UDPHeaderWrapper
    {
        public UDPHeader Header { get; set; }
        public DataWrapper2 Data { get; set; }

        public byte[] GetBytes()
        {
            return Header.GetBytes(Data.GetBytes());
        }

        public UDPHeaderWrapper ReadData(byte[] data)
        {
            byte[] udpData;
            return ReadData(Header = new UDPHeader().ReadHeader(data, out udpData), udpData);
        }

        public UDPHeaderWrapper ReadData(UDPHeader header, byte[] data)
        {
            if (header == null)
                return null;
            Data = new DataWrapper2().ReadData(data);
            return this;
        }
    }

    public struct DataWrapper2
    {
        public string Name { get; set; }
        public int Value { get; set; }

        public byte[] GetBytes()
        {
            List<byte> bytes = new List<byte>();
            byte[] nameBytes = new byte[16];
            int i = Encoding.ASCII.GetBytes(Name, 0, Name.Length, nameBytes, 0);
            bytes.AddRange(nameBytes);
            bytes.AddRange(BitConverter.GetBytes(Value));
            return bytes.ToArray();
        }

        public DataWrapper2 ReadData(byte[] data)
        {
            Name = Encoding.ASCII.GetString(data, 0, 16);
            Value = BitConverter.ToInt32(data, 16);
            return this;
        }
    }

    public static class Extension
    {
        
        public static int GetSum(this byte[] bytes)
        {
            int sum = 0;
            for(int i = 0; i < bytes.Length / 2; i+= 2)
            {
                sum += BitConverter.ToUInt16(bytes, i);
            }
            return sum;
        }
    }
    */

    /* TestCode
    * Console.WriteLine("Hello World!");
    UDPHeader header = new UDPHeader() { SourcePort = 65, DestinationPort = 66 };
    UDPHeaderWrapper wrapper = new UDPHeaderWrapper();
    wrapper.Header = header;
    wrapper.Data = new DataWrapper() { Name = "SPEED", Value = 40 };
    byte[] bytes = wrapper.GetBytes();

    UDPHeaderWrapper wrapperread = new UDPHeaderWrapper().ReadData(bytes);
    if (wrapperread != null)
    {
        Console.WriteLine("Success");
    }
    MemoryStream msStream = new MemoryStream();

    UDPStreamClient client = new UDPStreamClient(msStream, 100);
    UDPStreamClient client2 = new UDPStreamClient(msStream, 101);
    client.Connect(101);
    //byte[] bytes = Encoding.ASCII.GetBytes("Hello");
    byte[] bytes = (new DataWrapper() {SensorID = 4, Value = 45}).GetBytes();
    client.Send(bytes, bytes.Length);
    DataWrapper data = DataWrapper.ReadData(client2.Recive());*/
}
