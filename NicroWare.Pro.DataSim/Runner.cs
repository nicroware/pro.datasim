﻿using System;
using NicroWare.Lib.Networking.Manager;
using System.IO.Ports;
using NicroWare.Lib.Sensors;
using System.Collections.Generic;

namespace NicroWare.Pro.CarGuiDataSim
{
    public class Runner
    {
        public Runner()
        {
        }

        public static void Run()
        {
            SerialPort xbeePort = new SerialPort("/dev/ttyUSB1", 19200, Parity.None, 8, StopBits.One);
            UdpNetworkClient client = new UdpNetworkClient(xbeePort, 150);
            client.Connect(151);

            ushort[] sensorIDs = SensorLookup.SensorIDs;
            Dictionary<ushort, OBJWrap<DataWrapper>> generatedData = new Dictionary<ushort, OBJWrap<DataWrapper>>();

            double sinCounter = 0;
            int counter = 0;

            DateTime start = DateTime.Now;
            bool running = true;
            xbeePort.Open();
            while (xbeePort.CtsHolding && running)
            {
                byte[] tempArray = new byte[3 * (counter % 5 == 0 ? 8 : 2)];

                for (ushort i = 0; i < (counter % 5 == 0 ? 8 : 2); i++)
                {
                    if (!generatedData.ContainsKey(i))
                        generatedData.Add(i, new OBJWrap<DataWrapper>(new DataWrapper() { SensorID = i, Value = 0 }));
                    OBJWrap<DataWrapper> wrap = generatedData[i];
                    DataWrapper wrapper = wrap.obj;
                    //wrapper.Value = (int)Math.Abs(Math.Sin(counter) * 200);
                    wrapper.Value = counter;
                    wrapper.Value = wrapper.Value % 200;
                    wrap.obj = wrapper;
                    byte[] temp = wrap.obj.GetBytes(true);
                    Array.Copy(temp, 0, tempArray, i * 3, temp.Length);
                }

                try
                {
                    client.Send(tempArray, true);
                }
                catch
                {
                    running = false;
                }
                //Console.WriteLine("Message nr: " + counter + " bytes to write: " + port.BytesToWrite);
                Console.WriteLine("Time: {0}\tMsg nr: {1}\tbtw: {2}", (DateTime.Now - start).ToString("c"), counter, xbeePort.BytesToWrite);
                System.Threading.Thread.Sleep(200);
                counter++;
                sinCounter += 0.005;
            }
            xbeePort.Close();
            Console.WriteLine("CTS False error");
            Console.WriteLine("Runned for :" + (DateTime.Now - start).ToString());
            Console.ReadKey();
            return;
        }
    }

    /*public class OBJWrap<T>
    {
        public T obj { get; set; }

        public OBJWrap(T t)
        {
            this.obj = t;
        }
    }*/
}

